<br><br><br>
<h2>Plugin pour Telegraf</h2>

<div id="project">
    <pre>
        Dans le cadre d'un cours de Golang, il nous a été demandé de créer un plugin pour telegraf, récupérant les données générées dans un Powertop, afin de les envoyer dans une base InfluxDB.
        
        Réalisé avec la même équipe de GlobalNet, <b>excépté Kévin Amory</b>.
        
        Toutes les sources sont trouvables sur mon repo Framagit: </pre>
    &nbsp;&nbsp;<a href="https://framagit.org/math3r0se/powertop"><img src="images/framagit.png" alt="Framagit" style="width: 100px; height: 100px;"/></a>
    
</div>
