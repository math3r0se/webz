<h2>Photomaton</h2>
<div id="project">
    <pre>
        A l'occasion des portes ouvertes de Sup de Vinci en Février 2020, un défi a été lancé aux élèves de Bachelor de réalisé un programme de Photomaton pendant un cours de Python.
        
        Vous pouvez trouver les sources sur mon repo Framagit:
    </pre>
    &nbsp;&nbsp;<a href="https://framagit.org/math3r0se/photomaton"><img src="images/framagit.png" alt="Framagit" style="width: 100px; height: 100px;"/></a>
</div>