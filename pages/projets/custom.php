<h2>Ubuntu Custom</h2>

<div id="project">
    <pre>
        L'une de mes missions chez Vidal était de réaliser un ISO personnalisé et automatisé d'Ubuntu, ce projet m'ayant pris plusieurs mois et me tenant à coeur, j'ai décidé de le poursuivre en indépendant.
        J'ai l'ambition de créer un site permettant de choisir ses paramètres personnalisés, à l'instar de FAI-Project, qui m'a beaucoupé inspiré.

        Je n'ai pour le moment rien écrit, mais ça ne serait tarder.

        Toutes les sources sont trouvables sur mon repo Framagit:
    </pre>
    &nbsp;&nbsp;<a href="https://framagit.org/math3r0se/ubuntu-custom"><img src="images/framagit.png" alt="Framagit" style="width: 100px; height: 100px;"/></a>
</div>