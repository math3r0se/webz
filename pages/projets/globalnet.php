<h2>GlobalNet</h2>
<div id="project">
    <pre>
        GlobalNet est un projet d'études réalisé durant mon année de Bachelor Administration des Systèmes d'Informations, il avait pour but de simuler la réponse à un appel d'offre pour répondre au besoin d'une entreprise souhaitant refonder son infrastructure après un déménagement.
        
        Ce projet a été réalisé avec 4 de mes camarades :
    </pre>

        <div id="pres">
            <div id="projcenter">
                <img src="images/enzowintz.jpg" alt="Enzo Wintz"/>
                <h1>Enzo WINTZ</h1>
                <a href="https://www.linkedin.com/in/enzo-wintz-228079179/"><img id="link" src="images/linkedin.jpg" alt="Lien LinkedIn"/></a>
                <p>Responsable <b>Stockage</b>, <b>Sauvegarde</b> et <b>Service de fichiers</b></p>
            </div>

            <div id="projcenter">
                <img src="images/kevin.png" alt="Kévin Amory"/>
                <h1>Kévin AMORY</h1>
                <a href="https://www.linkedin.com/in/kevin-amory-107b43122/"><img id="link" src="images/linkedin.jpg" alt="Lien LinkedIn"/></a>
                <p>Responsable <b>Gestion de parcs</b>, <b>Télédistribution</b>, <b>Antivirus</b>, <b>Supervision</b>, <b>Base de données</b>, <b>Serveurs Web</b></p>
            </div>

            <div id="projcenter">
                <img src="images/leomollard.png" alt="Leo Mollard"/>
                <h1>Léo MOLLARD</h1>
                <a href="https://www.linkedin.com/in/l%C3%A9o-mollard-7538a4175/"><img id="link" src="images/linkedin.jpg" alt="Lien LinkedIn"/></a>
                <p><b>CHEF DE PROJET</b><br/><br/>Responsable <b>Infrastructure</b></p>
            </div>
            

            <div id="projcenter">
                <img src="images/nicolasbissieres.jpg" alt="Nicolas Bissières"/>
                <h1>Nicolas BISSIERES</h1>
                <a href="https://www.linkedin.com/in/nicolas-bissieres/"><img id="link" src="images/linkedin.jpg" alt="Lien LinkedIn"/></a>
                <p>Responsable <b>Active Directory</b> et <b>Messagerie</b></p>
            </div>

            <div id="projcenter">
                <img src="images/joeltordjman.jpg" alt="Joel Tordjman"/>
                <h1>Joël TORDJMAN</h1>
                <a href="https://www.linkedin.com/in/joel-tordjman/"><img id="link" src="images/linkedin.jpg" alt="Lien LinkedIn"/></a>
                <p>Responsable <b>Architecture réseau</b></p>
            </div>

            
        </div>

</div>