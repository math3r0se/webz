<div id="presentation">
    <h1>Présentation</h1>
    <p><i>Disclaimer : C'est dégueulasse, parce que j'ai fait ça en moins de 8h et from scratch, alors on se calme ! x)</i></p><br/>
    <p>
    Je m'appelle Joël Tordjman, j'ai 21 ans, je suis actuellement en Master Systèmes d'Informations, spécialité DevOps au sein de Sup de Vinci.<br/><br/>

    Je suis actuellement en poste en Alternance chez 3DS Outscale en tant qu'Administrateur Systèmes et Réseaux.<br/><br/>

    <b>Mon linkedin</b> —>  <a href="https://www.linkedin.com/in/joel-tordjman/"><img src="images/linkedin.jpg" alt="Mon LinkedIn" width="50" height="50"></a><br/><br/><br/>
    


    Je suis un habitué des environnements techniques:<br/><br/>

    - Ma première phase étant l'enfance, j'ai commencé à développer <b>Basic, C et en Assembleur</b> vers mes 5 ans grâce à de vieux livres du temps des études de mon père, puis le fameux tryptique du Web <b>(HTML, CSS, JavaScript)</b> grâce au Site du Zéro vers mes 9 ans.<br/><br/><br/>


    <b>-</b> Ensuite vient l'adolescence, je découvre ce concept de "backend" vers mes 12 ans avec les désormais légendaires <b>PHP, SQL, Node.JS et autre Ruby on Rails</b>, bien que le C reste mon langage de prédiléction.<br/><br/>

      Déjà concidéré comme "hasbeen" à cette époque, je me suis donc plongé malgré moi dans ces nouveaux langages qui font de la programmation orientée objets, tels que <b>Java ou C++</b>, puis sur le scripting avec le <b>Python, Rust et même Bash</b> (bien que c'est considéré comme une base).<br/><br/>

      C'est le moment ou l'on essaye de tout, je me met à essayer des dixaines de <b>frameworks comme Symfony ou CakePHP</b> dans tous les sens et même des services Cloud comme <b>AWS ou Azure</b>, je découvre ainsi les tous jeunes <b>Docker et Gitlab</b> qui venaient tout juste d'apparaître, que je n'ai pas arrêté d'utiliser jusqu'à aujourd'hui.<br/><br/><br/>


      <b>-</b> Vient finalement le temps d'arriver à l'âge adulte ou l'on commence à consolider ses acquis et à explorer un nouveau monde, on essaye de pousser ses limites avant la sénélité de la vieillesse ou l'on répète sans cesse "Si ça marche, pourquoi faire autrement ?"<br/><br/>

      Je commence à découvrir l'univers de la CI/CD vers les 16 ans, j'avais déjà entendu parler de ce <b>"Jenkins"</b> dont tout le monde causait, sans vraiment y prêter attention, puis vient le jour ou Gitlab avait sorti ses <b>runners</b>, et ce fût l'illumination, la vierge marie, un pokémon shiny, un signe du destin.<br/><br/>

      J'ai commencé doucement, histoire de découvrir, jusqu'à en devenir accro, comme un junkie en manque de sa dose, il fallait que je fasse ça, sinon mon projet n'avait aucun sens.<br/><br/><br/>


      <b>-</b> Et me voilà aujourd'hui, bien que je sois dans "la fleur de l'âge", j'ai énormément d'expériences et je passe aujourd'hui mon temps à enseigner à d'autres comment évoluer, mais surtout comment accepter l'échec et en faire une force, ce que j'ai du apprendre par moi-même<br/><br/>

      Je suis quelqu'un de plutôt curieux qui a toujours été solitaire, puis j'ai découvert l'univers du libre et de l'open-source et toute la communauté qui l'emporte, des passionnés qui n'ont qu'un seul but en tête, rendre la technologie accessible au plus grand nombre, en créant des projets utiles et bien documentés pour rendre la pratique démocratique, ne serait-ce que traîner sur StackOverflow est déjà un acte militant en soi.<br/><br/>

      Je ne fais plus autant de projets qu'avant, y ayant petit à petit perdu l'envie, je préfère aujourd'hui me concentrer sur le fait d'aider des néophytes à comprendre leurs solutions et à découvrir cet univers si vaste qui est malheuresement brimé par les multi-nationales qui nous répètent sans cesse qu'il faut être un génie pour faire de l'informatique ou des sciences.<br/><br/>
    </p>
</div>