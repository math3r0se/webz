<div id="experiences">
    <h1>Expériences</h1>
    
    <div class="expe_center">
        <div id="image">
            <img src="images/outscale.jpg" alt="3DS Outscale">
        </div>
        <div id="expe_text">
            <h1><b>Administrateur Systèmes et Réseaux</b></h1>
            <p>- Administration des serveurs répartis sur la planète</p><br/>
            <p>- Réalisation de scripts d'automatisation</p><br/>
            <p>- Participer à la gestion du cycle de vie de l'orchestrateur "<b>Tina</b>"</p><br/>
            <p>- Participer au MCO des plateformes</p><br/>
            <p>- Assurer le support niveau 1 et 2 des clients</p>
        </div>
        <div id="image" style="margin-left: 25px;">
            <img src="images/vidal.jpg" alt="Vidal France">
        </div>
        <div id="expe_text" style="margin-top: -5px;">
            <h1><b>Technicien Helpdesk</b></h1>
            <p>- Résoudre les incidents & Répondre aux demandes de supports</p><br/>
            <p>- Former les utilisateurs sur les applications et la messagerie</p><br/>
            <p>- Rédiger des procédures liées à certaines tâches de support</p><br/>
            <p>- Renouveler les postes, avec le transfert des données utilisateurs</p><br/>
            <p>- Participer à certaines tâches d'administration</p><br/>
            <p>- Améliorer et optimiser certaines tâches et services du support</p><br/>
            <p>- Mise en place d'une solution de déploiement automatisée Linux </p><br/>
        </div>
        <div id="image" style="margin-left: 25px;">
            <img src="images/kiddizy.jpg" alt="Kiddizy">
        </div>
        <div id="expe_text">
            <h1>Développeur Web</h1>
            <p>- Développement Web (Wordpress CMS, HTML, jQuery et CSS)</p><br/>
            <p>- Administration Cloud (AWS EC2, S3 et Lambda)</p><br/>
            <p>- Gestion de base de données MariaDB</p><br/>
        </div>
    </div>
    <br><br><br><br><br>
    <div class="expe_center">
        <div id="image">
            <img src="images/wefit.jpg" alt="WEFIT Group">
        </div>
        <div id="expe_text">
            <h1>Architecte réseau, Helpdesk</h1>
            <p>- Résoudre les incidents & Répondre aux demandes de supports</p><br/>
            <p>- Refonte du réseau câble avec routage BGP</p><br/>
        </div>
        <div id="image">
            <img src="images/nts2i.jpg" alt="NTS2i 'Au delà du Pc'">
        </div>
        <div id="expe_text">
            <h1>Technicien support et maintenance</h1>
            <p>Dépannage des clients pour tous types de demandes (virus, composants, réinstallation de l'OS, montage d'un PC sur mesure, etc...</p>
        </div>
        <div id="image">
            <img src="images/elite.jpg" alt="Elite Telecoms">
        </div>
        <div id="expe_text">
            <h1>Architecte réseau, Helpdesk</h1>
            <p>- Dépannage des utilisateurs (hardware et software)</p><br/>
            <p>- Maintenance du parc informatique</p><br/>
            <p>- Maintenance de plusieurs réseaux routés</p><br/>
            <p>- Maintenance d'un Windows Server</p><br/>
            <p>- Observation du déploiement de la fibre du NRO jusqu'au domicile du client (FTTH)</p><br/>
        </div>
    </div>
</div>