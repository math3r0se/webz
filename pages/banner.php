<div id="header">
    <h1>Joël Tordjman</h1>
    <ul>
        <li><a href="#presentation">Présentation</a></li>
        <li><a href="#experiences">Expériences</a></li>
        <li><a href="#projets">Projets</a></li>
        <li><a href="https://framagit.org/math3r0se/webz">Git</a></li>
    </ul>
</div>